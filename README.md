# Teamshirts frontend application task

- used create-react-app with typescript
- tailwindcss for basic styling
- react-router for basic routing

## Some Screenshots

| List page                 | Details page              |
| ------------------------- | ------------------------- |
| ![1](./screenshots/1.png) | ![2](./screenshots/2.png) |
| ![3](./screenshots/3.png) | ![5](./screenshots/5.png) |

Thanks for reviewing this code.

Yasser Zadeh
