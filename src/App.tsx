import { QueryClient, QueryClientProvider } from "react-query";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { BreedDetails } from "./routes/BreedDetails";
import { CatList } from "./routes/CatList";
import ErrorPage from "./routes/ErrorPage";

const queryClient = new QueryClient();

const router = createBrowserRouter([
  { path: "/cats", element: <CatList />, errorElement: <ErrorPage /> },
  {
    path: "/cats/:breedId",
    element: <BreedDetails />,
    errorElement: <ErrorPage />,
  },
]);

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <div className="bg-slate-200 min-h-screen leading-relaxed text-gray-800">
        <div className="container mx-auto md:px-8">
          <main>
            <RouterProvider router={router} />
          </main>
        </div>
      </div>
    </QueryClientProvider>
  );
}

export default App;
