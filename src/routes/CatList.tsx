import { useQuery } from "react-query";
import { Link, useSearchParams } from "react-router-dom";
import { getCats } from "../api";
import { Cat } from "../types";
import { range } from "../utils";

export function CatList() {
  const [searchParams] = useSearchParams();
  const page = Number(searchParams.get("page") ?? 1);
  const queryResult = useQuery<Cat[]>(["cats", page], () => getCats({ page }), {
    keepPreviousData: true,
  });

  const cats = queryResult.data ?? [];

  return (
    <article className="flex flex-col items-center min-h-screen px-4">
      <h1 className="text-3xl font-bold p-8 leading-tight text-gray-900">
        Cats
      </h1>
      <section className="gap-8 space-y-8 columns-2 sm:columns-3 grow w-full">
        {cats.map((cat) => {
          const breed = cat.breeds?.[0];
          return (
            <Link
              key={cat.id}
              to={breed ? `/cats/${breed?.id}` : "#"}
              className="block rounded-xl"
            >
              <article className="rounded-xl drop-shadow-md bg-white hover:bg-sepia-200 break-inside-avoid-column text-gray-500">
                <img
                  alt=""
                  src={cat.url}
                  style={{ aspectRatio: cat.width / cat.height }}
                  className="w-full object-cover rounded-t-lg sepia hover:filter-none"
                ></img>
                <h2 className="text-center text-md font-medium p-1">
                  {breed?.name ?? "Breed not specified"}
                </h2>
              </article>
            </Link>
          );
        })}
      </section>
      <section>
        <span className={queryResult.isFetching ? "visible" : "invisible"}>
          loading...
        </span>
      </section>
      <nav className="my-8 flex flex-nowrap">
        {range(1, 10).map((_page) => (
          <Link
            key={_page}
            to={`/cats?page=${_page}`}
            className={
              "relative inline-flex items-center border border-gray-300 bg-white first:rounded-l-lg last:rounded-r-lg px-3 md:px-4 py-2 text-sm font-medium text-gray-500 hover:bg-sepia-200 focus:z-20 " +
              (_page === page ? " bg-sepia-500 text-white" : "")
            }
          >
            {_page}
          </Link>
        ))}
      </nav>
    </article>
  );
}
