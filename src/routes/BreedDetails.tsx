import { useQuery } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import { getCatBread } from "../api";

export function BreedDetails() {
  const { breedId } = useParams();
  const navigate = useNavigate();

  if (!breedId) {
    throw new Error();
  }

  const { data: cat, isLoading } = useQuery(["cats", breedId], () =>
    getCatBread(breedId!)
  );

  if (isLoading) {
    return <div>loading...</div>;
  }

  const breed = cat.breeds?.[0];
  if (!breed) return <div>No breed found</div>;

  return (
    <div className="flex flex-col gap-2 relative">
      <button
        role="link"
        tabIndex={0}
        onClick={() => navigate(-1)}
        className="outline-0 absolute top-4 left-4 md:static md:my-2 flex justify-center items-center rounded-full w-10 h-10 bg-white text-slate-800  focus:bg-sepia-300 hover:bg-sepia-300"
      >
        <span aria-label="Go back" className="text-xl relative -top-0.5">
          ←
        </span>
      </button>
      <article className="flex flex-col md:flex-row gap-4">
        <img
          src={cat.url}
          alt=""
          style={{ aspectRatio: cat.width / cat.height }}
          className="rounded-b-lg md:rounded-lg object-cover w-full h-full md:w-1/3"
        ></img>
        <div className="px-0 flex flex-col gap-5">
          <h1 className="text-3xl font-bold text-center md:text-left text-slate-700">
            {breed.name}
          </h1>
          <p className="text-lg text-slate-600 text-center md:text-left">
            {breed.temperament}
          </p>
          <div className="flex flex-row justify-center md:justify-start gap-4">
            <div className="flex flex-col items-center bg-white shadow-md rounded-lg p-2 text-center">
              <div className="text-slate-400">Life Expectancy</div>
              <div className="text-xl whitespace-nowrap my-1 text-sepia-800">
                {breed.life_span}
              </div>
              <div className="text-slate-400 text-md">years</div>
            </div>
            <div className="flex flex-col items-center bg-white shadow-md rounded-lg p-2 text-center">
              <div className="text-slate-400 text-md">Average Weight</div>
              <div className="text-xl whitespace-nowrap my-1 text-sepia-800">
                {breed.weight.metric}
              </div>
              <div className="text-slate-400 text-md">Kilos (kg)</div>
            </div>
          </div>
        </div>
      </article>
    </div>
  );
}
