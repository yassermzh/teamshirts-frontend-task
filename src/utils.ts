export const range = (from: number, to: number): number[] => {
  return Array.from({ length: to - from + 1 }).map((_, i) => i + from);
};
