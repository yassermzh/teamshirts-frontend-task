export type CatWeight = { imperial: string; metric: string };

export type CatBreed = {
  id: string;
  name: string;
  temperament: string;
  weight: CatWeight;
  life_span: string;
} & Record<string, any>;

export type Cat = {
  id: string;
  url: string;
  width: number;
  height: number;
  breeds?: CatBreed[];
};
