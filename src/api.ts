import { Cat, CatBreed } from "./types";

export const getCats = (options: { page: number }): Promise<Cat[]> =>
  fetch(
    "https://api.thecatapi.com/v1/images/search?" +
      new URLSearchParams({
        page: `${options.page}`,
        order: "ASC",
        limit: "6",
        size: "small",
        has_breeds: "1",
      }),
    {
      headers: { "x-api-key": process.env.REACT_APP_API_KEY ?? "" },
    }
  )
    .then((res) => res.json())
    .then((cats) => cats.filter((cat: Cat) => (cat.breeds ?? []).length > 0));

export const getCatBread = (id: CatBreed["id"]) =>
  fetch(
    "https://api.thecatapi.com/v1/images/search?" +
      new URLSearchParams({
        page: "1",
        order: "ASC",
        limit: "1",
        size: "small",
        breed_ids: id,
      }),
    {
      headers: { "x-api-key": process.env.REACT_APP_API_KEY ?? "" },
    }
  )
    .then((res) => res.json())
    .then((cats) => {
      if (cats?.length !== 1) return null;
      return cats[0];
    });
