/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        sepia: {
          900: "#693317",
          800: "#8c441e",
          700: "#af5526",
          600: "#d2662d",
          500: "#da7f50",
          400: "#e19973",
          300: "#e9b396",
          200: "#f0ccb9",
          100: "#f8e6dc",
        },
      },
    },
  },
  plugins: [],
};
